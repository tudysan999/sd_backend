package ro.tuc.ds2020.services;


import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.builders.QuestionBuilder;
import ro.tuc.ds2020.dtos.QuestionDTO;
import ro.tuc.ds2020.entities.Question;
import ro.tuc.ds2020.repositories.QuestionRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class QuestionService {


    private final QuestionRepository questionRepository;


    public QuestionService(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    public List<QuestionDTO> getAllQuestions() {
        List<Question> questionList = questionRepository.findAll();


        return questionList.stream().map(QuestionBuilder::toDTO).collect(Collectors.toList());
    }

    public QuestionDTO getQuestionById(long id) {
        Optional<Question> questionOptional = questionRepository.findById(id);

        if (!questionOptional.isPresent()) {
            throw new ResourceNotFoundException(Question.class.getSimpleName() + " with id: " + id);
        }

        return QuestionBuilder.toDTO(questionOptional.get());
    }

    public List<QuestionDTO> getQuestionsByUserId(long userId) {
        List<Question> questionsByUser = questionRepository.findAll().stream().filter(q -> q.getAuthor().getId() == userId).collect(Collectors.toList());

        return questionsByUser.stream().map(QuestionBuilder::toDTO).collect(Collectors.toList());
    }


    public QuestionDTO saveQuestion(QuestionDTO question) {
        return QuestionBuilder.toDTO(questionRepository.save(QuestionBuilder.toEntity(question)));
    }

    public QuestionDTO updateQuestion(long id, QuestionDTO questionDTO) {
        Optional<Question> questionOptional = questionRepository.findById(id);

        if (!questionOptional.isPresent()) {
            throw new ResourceNotFoundException(Question.class.getSimpleName() + " with id: " + id);
        }

        Question initialQuestion = questionOptional.get();

        initialQuestion.setTitle(questionDTO.getTitle());
        initialQuestion.setText(questionDTO.getText());

        return QuestionBuilder.toDTO(questionRepository.save(initialQuestion));
    }

    public void deleteQuestion(long id) {
        questionRepository.deleteById(id);
    }


}
