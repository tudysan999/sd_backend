package ro.tuc.ds2020.builders;

import ro.tuc.ds2020.dtos.TagDTO;
import ro.tuc.ds2020.entities.Tag;

public class TagBuilder {

    public static TagDTO toDTO(Tag tag) {
        return new TagDTO(tag.getId(), tag.getName());
    }


    public static Tag toEntity(TagDTO tagDTO) {
        return new Tag(tagDTO.getId(), tagDTO.getName());
    }
}
