package ro.tuc.ds2020.builders;

import ro.tuc.ds2020.dtos.AnswerDTO;
import ro.tuc.ds2020.entities.Answer;

public class AnswerBuilder {

    public static AnswerDTO toDTO(Answer answer) {
        return new AnswerDTO(answer.getId(), UserBuilder.toDTO(answer.getAuthor()), answer.getText(), answer.getUpvotes(), QuestionBuilder.toDTO(answer.getQuestion()));
    }

    public static Answer toEntity(AnswerDTO answerDTO) {
        return new Answer(answerDTO.getId(), UserBuilder.toEntity(answerDTO.getAuthor()), answerDTO.getText(), answerDTO.getUpvotes(), QuestionBuilder.toEntity(answerDTO.getQuestion()));
    }
}
