package ro.tuc.ds2020.dtos;

import ro.tuc.ds2020.entities.Question;
import ro.tuc.ds2020.entities.User;

public class AnswerDTO {

    private long id;
    private UserDTO author;
    private String text;
    private int upvotes;
    private QuestionDTO question;

    public AnswerDTO() {
    }

    public AnswerDTO(long id, UserDTO author, String text, int upvotes, QuestionDTO question) {
        this.id = id;
        this.author = author;
        this.text = text;
        this.upvotes = upvotes;
        this.question = question;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserDTO getAuthor() {
        return author;
    }

    public void setAuthor(UserDTO author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getUpvotes() {
        return upvotes;
    }

    public void setUpvotes(int upvotes) {
        this.upvotes = upvotes;
    }

    public QuestionDTO getQuestion() {
        return question;
    }

    public void setQuestion(QuestionDTO question) {
        this.question = question;
    }
}
